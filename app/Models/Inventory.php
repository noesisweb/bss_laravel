<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;


class Inventory extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
  /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sku_id',
        'inventory_date',
        'order_capacity',
    ];
  

    protected $table = 'inventories';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function prod()
    {
        return $this->belongsTo(\App\Models\Product::class,'sku_id','sku_id');
    }
   

    /*public function setOrderCapacityAttribute($values){
        $order_cap=$values;
        $sku=$this->attributes['sku_id'];
        $order_date=$this->attributes['inventory_date'];
        if(DB::table('products')->where('id', $sku)->exists()){
            $product_details=DB::table('products')->where('id', $sku)->first();
            $sku_id=$product_details->sku_id;
            if(DB::table('orders')->where('order_date', $order_date)->where('sku_id', $sku_id)->exists()){
                $updated_created= date("Y-m-d H:i:s");
                if(DB::table('inventories')->where('sku_id', $sku_id)->where('inventory_date', $order_date)->count()!=0){
                    $order_latest=DB::table('orders')->where('order_date', $order_date)->where('sku_id', $sku_id)->first();
                    $latest_entry=DB::table('inventories')->where('sku_id', $sku_id)->where('inventory_date', $order_date)->orderby('id','DESC')->first();
                    if($order_cap>$latest_entry->order_capacity){
                        $new_cap=$order_cap-($latest_entry->order_capacity);
                        $totl_cap=($order_latest->capacity_left)+$new_cap;
                    }else{
                        $new_cap=($latest_entry->order_capacity)-$order_cap;
                        $totl_cap=($order_latest->capacity_left)-$new_cap;
                       if($totl_cap<0)
                       {
                           $chc=($latest_entry->order_capacity)-($order_latest->capacity_left);
                        throw ValidationException::withMessages(['order_capacity' => 'You cannot enter value less then ordered quantity left. You can enter minimum value of '.$chc.' to make capacity left zero.']);                       }
                        
                    }
                    DB::update('update orders set capacity_left= ?, updated_at = ? where sku_id = ? and order_date = ?',[$totl_cap,$updated_created,$sku_id,$order_date]);

            }else{
                $order_latest=DB::table('orders')->where('order_date', $order_date)->where('sku_id', $sku_id)->first();
                if($order_cap>$order_latest->capacity_left){
                    $new_cap=$order_cap-($order_latest->capacity_left);
                    $totl_cap=($order_latest->capacity_left)+$new_cap-($order_latest->order_quantity);
                }else{
                    $totl_cap=$order_cap-($order_latest->order_quantity);
                }
                DB::update('update orders set capacity_left= ?, updated_at = ? where sku_id = ? and order_date = ?',[$totl_cap,$updated_created,$sku_id,$order_date]);

            }       
    }
}
    $this->attributes['order_capacity'] =$order_cap;
}*/


public function setOrderCapacityAttribute($values){
    $order_cap=$values;
    $sku=$this->attributes['sku_id'];
    $order_date=$this->attributes['inventory_date'];
    if(DB::table('products')->where('id', $sku)->exists()){
        $product_details=DB::table('products')->where('id', $sku)->first();
        $sku_id=$product_details->sku_id;
        if(DB::table('orders')->where('order_date', $order_date)->where('sku_id', $sku_id)->exists()){
            $updated_created= date("Y-m-d H:i:s");
            if(DB::table('inventories')->where('sku_id', $sku_id)->where('inventory_date', $order_date)->count()!=0){
                $order_latest=DB::table('orders')->where('order_date', $order_date)->where('sku_id', $sku_id)->first();
                $latest_entry=DB::table('inventories')->where('sku_id', $sku_id)->where('inventory_date', $order_date)->orderby('id','DESC')->first();
                $totl_cap=($order_latest->capacity_left)+$order_cap;
                   if($totl_cap<0)
                   {
                       $chc=($latest_entry->order_capacity)-($order_latest->capacity_left);
                    throw ValidationException::withMessages(['order_capacity' => 'You cannot enter value less then ordered quantity left. You can enter minimum value of '.$chc.' to make capacity left zero.']);                    
                   }
                    
                
                DB::update('update orders set capacity_left= ?, updated_at = ? where sku_id = ? and order_date = ?',[$totl_cap,$updated_created,$sku_id,$order_date]);

        }else{
            /*
            $order_latest=DB::table('orders')->where('order_date', $order_date)->where('sku_id', $sku_id)->first();
            if($order_cap>$order_latest->capacity_left){
                $new_cap=$order_cap-($order_latest->capacity_left);
                $totl_cap=($order_latest->capacity_left)+$new_cap-($order_latest->order_quantity);
            }else{
                $totl_cap=$order_cap-($order_latest->order_quantity);
            }
            DB::update('update orders set capacity_left= ?, updated_at = ? where sku_id = ? and order_date = ?',[$totl_cap,$updated_created,$sku_id,$order_date]);
        */



            $order_latest=DB::table('orders')->where('order_date', $order_date)->where('sku_id', $sku_id)->first();
            $latest_entry=DB::table('inventories')->where('sku_id', $sku_id)->where('inventory_date', $order_date)->orderby('id','DESC')->first();
            $totl_cap=$order_cap;
               if($totl_cap<0)
               {
                   $chc=$order_latest->capacity_left;
                throw ValidationException::withMessages(['order_capacity' => 'You cannot enter value less then ordered quantity left. You can enter minimum value of '.$chc.' to make capacity left zero.']);                       }
                
            
            DB::update('update orders set capacity_left= ?, updated_at = ? where sku_id = ? and order_date = ?',[$totl_cap,$updated_created,$sku_id,$order_date]);


        }       
}
}
$this->attributes['order_capacity'] =$order_cap;
}


/*
public function setOrderCapacityAttribute($values){
    $order_cap=$values;
    $sku=$this->attributes['sku_id'];
    $order_date=$this->attributes['inventory_date'];
    $increase_or_decrease=$this->attributes['order_quantity'];
    if(DB::table('products')->where('id', $sku)->exists()){
        $product_details=DB::table('products')->where('id', $sku)->first();
        $sku_id=$product_details->sku_id;
        if(DB::table('orders')->where('order_date', $order_date)->where('sku_id', $sku_id)->exists()){
            $updated_created= date("Y-m-d H:i:s");
            
            if($increase_or_decrease == 0){
                $order_latest=DB::table('orders')->where('order_date', $order_date)->where('sku_id', $sku_id)->first();
                $latest_entry=DB::table('inventories')->where('sku_id', $sku_id)->where('inventory_date', $order_date)->orderby('id','DESC')->first();
                $totl_cap=($order_latest->capacity_left)+$order_cap;
                DB::update('update orders set capacity_left= ?, updated_at = ? where sku_id = ? and order_date = ?',[$totl_cap,$updated_created,$sku_id,$order_date]);

            }else if($increase_or_decrease == 1){
                $order_latest=DB::table('orders')->where('order_date', $order_date)->where('sku_id', $sku_id)->first();
                $latest_entry=DB::table('inventories')->where('sku_id', $sku_id)->where('inventory_date', $order_date)->orderby('id','DESC')->first();
                $totl_cap=($order_latest->capacity_left)-$order_cap;
                if($totl_cap<0)
                {
                    $chc=$order_latest->capacity_left;
                 throw ValidationException::withMessages(['order_capacity' => 'You cannot enter value less then ordered quantity left. You can enter minimum value of '.$chc.' to make capacity left zero.']);                    
                }
                 
                DB::update('update orders set capacity_left= ?, updated_at = ? where sku_id = ? and order_date = ?',[$totl_cap,$updated_created,$sku_id,$order_date]);

            }    
}
}
$this->attributes['order_capacity'] =$order_cap;
}
*/

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
   
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
