<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\InventoryRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class InventoryCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class InventoryCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Inventory::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/inventory');
        CRUD::setEntityNameStrings('inventory', 'inventories');
      
        $this->crud->addColumn([
            'label' => "Title",
            'type' => 'select',
            'name' => 'title', // the db column for the foreign key
            'attribute' => 'title', // foreign key attribute that is shown to user
            'entity' => 'prod',
            // optional
            'model' => "App\Models\Product", // foreign key model
        ]);
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->removeButton('update');
        //CRUD::setFromDb(); // columns
        $this->crud->removeColumn("order_quantity");
		$this->crud->column('title');
		$this->crud->column('inventory_date');
		$this->crud->column('sku_id');
		$this->crud->column('order_capacity');
		$this->crud->disableResponsiveTable();
		
		$this->crud->addFilter([ 
		  'type'  => 'date',
		  'name'  => 'inventory_date',
		  'label' => 'Inventory Date'
		],
		false, // the simple filter has no values, just the "Draft" label specified above
		function($value) { // if the filter is active (the GET parameter "draft" exits)
		  $this->crud->addClause('where', 'inventory_date', $value); 
		  // we've added a clause to the CRUD so that only elements with draft=1 are shown in the table
		  // an alternative syntax to this would have been
		  // $this->crud->query = $this->crud->query->where('draft', '1'); 
		  // another alternative syntax, in case you had a scopeDraft() on your model:
		  // $this->crud->addClause('draft'); 
		});
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        $this->crud->setValidation(InventoryRequest::class);

        $this->crud->addField([
            'label' => "SKU",
            'type' => 'select2',
            'name' => 'sku_id', // the db column for the foreign key
            'attribute' => 'title', // foreign key attribute that is shown to user
            'entity' => 'prod',
            // optional
            'model' => "App\Models\Product", // foreign key model
           
           
        ]);
        $this->crud->addField([
            'label' => "Date",
            'type' => 'date_picker',
            'name' => 'inventory_date', // the db column for the foreign key
            'placeholder' => 'dd/mm/yyyy', // Doesn't work
            'date_picker_options' => [
                'todayBtn' => 'linked',
                'format'   => 'dd-mm-yyyy'
             ],
           
        ]);
       /* $this->crud->addField([   // radio
            'label'       => 'Do you want to', // the input label
            'type'        => 'radio',
            'name'     => 'order_quantity',
           
            'options'     => [
                // the key will be stored in the db, the value will be shown as label; 
                0 => "Increase",
                1 => "Decrease",
                2 => "New Entry"
            ],
            // optional
            //'inline'      => false, // show the radios all on the same line?
        ]);
        */ 
        $this->crud->addField([
            'label' => "Quantity Cap",
            'type' => 'number',
            'name' => 'order_capacity', // the db column for the foreign key
            'attributes' => [
                'min' => 0,
                'max' => 1000,
            ],          
        ]);
       
        
        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    /*
    protected function setupShowOperation()
    { 
        $this->crud->set('show.setFromDb', false);
        $this->crud->addColumn("sku_id");
        $this->crud->addColumn("inventory_date");
        $this->crud->addColumn("order_capacity");
        $this->crud->removeButton('update');

    }
*/
    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {           $this->crud->addField([
            'label' => "SKU",
            'type' => 'text',
            'name' => 'sku_id', // the db column for the foreign key
           
        ]);
        $this->crud->addField([
            'label' => "Date",
            'type' => 'date_picker',
            'name' => 'inventory_date', // the db column for the foreign key
            'placeholder' => 'dd/mm/yyyy', // Doesn't work
            'date_picker_options' => [
                'todayBtn' => 'linked',
                'format'   => 'dd-mm-yyyy'
             ],
           
        ]);
        $this->crud->addField([
            'label' => "Quantity Cap",
            'type' => 'number',
            'name' => 'order_capacity', // the db column for the foreign key
            'attributes' => [
                'min' => 0,
                'max' => 1000,
            ],          
        ]);
    }
}
