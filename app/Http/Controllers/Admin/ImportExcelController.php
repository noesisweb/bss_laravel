<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Excel;
use App\import;
use DateTime;
use Illuminate\Validation\ValidationException;


class ImportExcelController extends Controller
{
    function index()
    {
     $data = DB::table('inventories')->orderBy('inventory_date', 'DESC')->get();
     return view(backpack_view('import_excel', compact('data')));
    }

    function import(Request $request)
    {
    try{
     $data =  Excel::toArray(new Inventoryimport, request()->file('select_file'));
     $count=count($data[0]);
     $values=array();
     for($i=1;$i<$count;$i++){
        $row=$data[0][$i];
        for($j=1;$j<count($row);$j++){
            $sku_id=$row[0];
            $capacity=$row[$j];
            $inventory_date= \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($data[0][0][$j]);
            $datetime = new DateTime(); 

            $updated_created= $datetime->format('Y-m-d h:m:s');
            $date1 = new DateTime("1970-01-01"); 
            if($inventory_date!=$date1 && $sku_id!=null){
            array_push($values,array('sku_id'=>$sku_id ,'inventory_date'=> $inventory_date,'order_capacity'=> $capacity,'updated_at'=>$updated_created,'created_at'=>$updated_created));
            }
            //$values= array('sku_id'=> $sku_id,'inventory_date'=> $inventory_date,'order_capacity'=> $capacity,'updated_at'=>$updated_created,'created_at'=>$updated_created);
           
        }
     }
    }catch(\Exception $e){
        throw ValidationException::withMessages(['order_capacity' => 'There was some error due to which uploading was not successfull. Please check the excel file.']);
    }

     DB::beginTransaction();

try {
    DB::table('inventories')->insert($values);
    DB::commit();
    // all good
} catch (\Exception $e) {
    DB::rollback();
    throw ValidationException::withMessages(['order_capacity' => 'There was some error due to which uploading was not successfull. Please check the excel file.']);               
        }

    // something went wrong


     
   
     return back()->with('success', 'Excel Data Imported successfully.');
    }
}