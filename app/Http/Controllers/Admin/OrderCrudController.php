<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\OrderRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class OrderCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class OrderCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Order::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/order');
        CRUD::setEntityNameStrings('order', 'orders');
        $this->crud->addColumn([
            'label' => "Title",
            'type' => 'select',
            'name' => 'title', // the db column for the foreign key
            'attribute' => 'title', // foreign key attribute that is shown to user
            'entity' => 'prod',
            // optional
            'model' => "App\Models\Product", // foreign key model
        ]);
        $this->crud->enableExportButtons();
        
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        //CRUD::setFromDb(); // columns
        $this->crud->removeButton('create');
        $this->crud->removeButton('update');
        $this->crud->removeButton('delete');
		$this->crud->column('title');
		$this->crud->column('order_date');
		$this->crud->column('sku_id');
		$this->crud->column('order_quantity');
		$this->crud->column('capacity_left');
		$this->crud->disableResponsiveTable();
		
		$this->crud->addFilter([ 
		  'type'  => 'date',
		  'name'  => 'order_date',
		  'label' => 'Order Date'
		],
		false, // the simple filter has no values, just the "Draft" label specified above
		function($value) { // if the filter is active (the GET parameter "draft" exits)
		  $this->crud->addClause('where', 'order_date', $value); 
		  // we've added a clause to the CRUD so that only elements with draft=1 are shown in the table
		  // an alternative syntax to this would have been
		  // $this->crud->query = $this->crud->query->where('draft', '1'); 
		  // another alternative syntax, in case you had a scopeDraft() on your model:
		  // $this->crud->addClause('draft'); 
		});
		
        //$this->crud->removeColumn('capacity_left');

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(OrderRequest::class);

        CRUD::setFromDb(); // fields

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
