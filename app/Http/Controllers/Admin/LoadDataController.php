<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

class LoadDataController extends Controller
{
    public function __construct()
    {
       // $this->middleware(backpack_middleware());
    }
    public function metadata_callout($prod_id,$variant_id){
        
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://5fafec0b5211ef46f5253fa3778ac4c1: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@bombay-sweet-shop.myshopify.com/admin/products/".$prod_id."/variants/".$variant_id."/metafields.json",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Cookie: _master_udr=eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaEpJaWswWVdVeVpHWXpNQzFtWmpZMUxUUTBaV010WVRKaE5TMWlOekZpTVROa1l6aGpNV1FHT2daRlJnPT0iLCJleHAiOiIyMDIyLTEwLTA4VDA1OjE1OjAyLjA2MVoiLCJwdXIiOiJjb29raWUuX21hc3Rlcl91ZHIifX0%3D--4dd1fadf47466efc3ca05b56483d10b1820a0f74; _secure_admin_session_id_csrf=d6afb78daa192a81b93acc2c43237d0e; _secure_admin_session_id=d6afb78daa192a81b93acc2c43237d0e; __cfduid=d6a094b4fbc025dee7e9494056d9951811600928003; _shopify_y=abd876b3-6bde-4126-a5b6-8e18583b4fd8; _y=abd876b3-6bde-4126-a5b6-8e18583b4fd8"
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $data = json_decode($response, true);
        return $data;
    }
    //
    public function index()
    {
        
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://5fafec0b5211ef46f5253fa3778ac4c1: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@bombay-sweet-shop.myshopify.com/admin/api/2020-07/products.json?limit=250",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Cookie: _master_udr=eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaEpJaWswWVdVeVpHWXpNQzFtWmpZMUxUUTBaV010WVRKaE5TMWlOekZpTVROa1l6aGpNV1FHT2daRlJnPT0iLCJleHAiOiIyMDIyLTEwLTA4VDA1OjE1OjAyLjA2MVoiLCJwdXIiOiJjb29raWUuX21hc3Rlcl91ZHIifX0%3D--4dd1fadf47466efc3ca05b56483d10b1820a0f74; _secure_admin_session_id_csrf=d6afb78daa192a81b93acc2c43237d0e; _secure_admin_session_id=d6afb78daa192a81b93acc2c43237d0e; __cfduid=d6a094b4fbc025dee7e9494056d9951811600928003; _shopify_y=abd876b3-6bde-4126-a5b6-8e18583b4fd8; _orig_referrer=https%3A%2F%2Fbombay-sweet-shop.myshopify.com%2Fadmin%2Fapi%2F2020-04%2Fgraphql.json; _landing_page=%2Fadmin%2Fauth%2Flogin; _y=abd876b3-6bde-4126-a5b6-8e18583b4fd8; identity-state=BAh7BkkiJTcxZjgyMTI1OTFiMTdhYjYzZDk0NmNmMTdiZTBkN2U2BjoGRUZ7C0kiDnJldHVybi10bwY7AFRJIj1odHRwczovL2JvbWJheS1zd2VldC1zaG9wLm15c2hvcGlmeS5jb20vYWRtaW4vYXV0aC9sb2dpbgY7AFRJIhFyZWRpcmVjdC11cmkGOwBUSSJJaHR0cHM6Ly9ib21iYXktc3dlZXQtc2hvcC5teXNob3BpZnkuY29tL2FkbWluL2F1dGgvaWRlbnRpdHkvY2FsbGJhY2sGOwBUSSIQc2Vzc2lvbi1rZXkGOwBUOgxhY2NvdW50SSIPY3JlYXRlZC1hdAY7AFRmFzE2MDIxMzQxMDIuMDg4MzM3N0kiCm5vbmNlBjsAVEkiJTRhNWM3MDdhZDYzY2Q2NDU2MmRkMTg3NjEwYzFmNmQ3BjsARkkiCnNjb3BlBjsAVFsLSSIKZW1haWwGOwBUSSI3aHR0cHM6Ly9hcGkuc2hvcGlmeS5jb20vYXV0aC9kZXN0aW5hdGlvbnMucmVhZG9ubHkGOwBUSSILb3BlbmlkBjsAVEkiDHByb2ZpbGUGOwBUSSJOaHR0cHM6Ly9hcGkuc2hvcGlmeS5jb20vYXV0aC9wYXJ0bmVycy5jb2xsYWJvcmF0b3ItcmVsYXRpb25zaGlwcy5yZWFkb25seQY7AFRJIjBodHRwczovL2FwaS5zaG9waWZ5LmNvbS9hdXRoL2JhbmtpbmcubWFuYWdlBjsAVA%3D%3D--9a135f7739711868403db8847e7d86fc99aa9b09"
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $data = json_decode($response, true);
        

        DB::table('products')->truncate();
   


        $datt['count']=count($data['products']);
        $counts=count($data['products']);
        for($i=0;$i<$counts;$i++){
            $title=$data['products'][$i]['title'];
            for($k=0;$k<count($data['products'][$i]['variants']);$k++){
                if(count($data['products'][$i]['variants'])>1){
                    $title_var=$title." - ".$data['products'][$i]['variants'][$k]['title'];
                }else{
                    $title_var=$data['products'][$i]['title'];
                }
                $prod_id=$data['products'][$i]['variants'][$k]['product_id'];
                $variant_id=$data['products'][$i]['variants'][$k]['id'];
                $sku_id=$data['products'][$i]['variants'][$k]['sku'];
                $inventory=$data['products'][$i]['variants'][$k]['inventory_quantity'];
                $metadatainfo=$this->metadata_callout($prod_id,$variant_id);
                
                $bundle_sku=NUll;
                $bundle_quantity=NULL;
                $total_bundle=NULL;
                $bundled=0;
                $string_bundle=NULL;
                if(array_key_exists('metafields',$metadatainfo)){
	                $metacount=count($metadatainfo['metafields']);
	                for($j=0;$j<$metacount;$j++){
	                    if($metadatainfo['metafields'][$j]['key']=="bundle-sku"){
	                        $bundle_sku=json_decode($metadatainfo['metafields'][$j]['value']);
	                        $bundled=1;
	                    }elseif($metadatainfo['metafields'][$j]['key']=="bundle-quantity"){
	                        $bundle_quantity=json_decode($metadatainfo['metafields'][$j]['value']);
	                    }
	                }
                }
                if($bundled == 1){
                    $total_bundle = array_combine($bundle_sku, $bundle_quantity);
                    $string_bundle=http_build_query($total_bundle) . "\n";
                }
                if($sku_id != Null){
                    $values= array('title'=> $title_var,'product_id'=> $variant_id,'sku_id'=> $sku_id,'inventory'=> $inventory,'bundled'=>$bundled,'bundled_sku'=>$string_bundle);
                    DB::table('products')->insert($values);
                }
            }
        }


        return view(backpack_view('loaded_success'), $datt);
    }
}
