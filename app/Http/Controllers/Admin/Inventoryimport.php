<?php

namespace App\Http\Controllers\Admin;

use App\models\Inventory;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\HeadingRowImport;

class Inventoryimport implements ToModel
{
    /**
     * @param array $row
     *
     * @return User|null
     */

    
  

    public function model(array $row)
    {
       
            return new Inventory([
           'sku_id'     => $row[0],
           'inventory_date'    =>  \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[1]) ,
           'order_capacity' => $row[2],
           'order_quantity' => $row[3],
        ]);
    }
}