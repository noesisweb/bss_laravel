<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Excel;
use App\import;
use DateTime;

class OrderCreatorController extends Controller
{


    function import(Request $request)
    {
    
      /*  $json = file_get_contents('php://input');
        $action = json_decode($json, true);
        $count_note=count($action['note_attributes']);
        $order_date=date('Y-m-d', strtotime(' + 2 days'));
        for($i=0;$i<$count_note;$i++){
            if( $action['note_attributes'][$i]['name'] == "date"){
                $var = $action['note_attributes'][$i]['value'];
                $date = str_replace('/', '-', $var);
                $order_date=date('Y-m-d', strtotime($date));
            }
        }
        
        $order_id=$action['id'];
        $count_order=count($action['line_items']);
        for($i=0;$i<$count_order;$i++){
            $sku=$action['line_items'][$i]['sku'];
            $quantity=$action['line_items'][$i]['quantity'];
            $is_bundle=0;
            if(DB::table('products')->where('sku_id', $sku)->exists()){
            $bundle_checker=DB::table('products')->where('sku_id', $sku)->first();
            $is_bundle=$bundle_checker->bundled;
            }
            if($is_bundle == 0){
            if(DB::table('orders')->where('order_date', $order_date)->where('sku_id', $sku)->exists()){
                $orders=DB::table('orders')->where('order_date', $order_date)->where('sku_id', $sku)->first();
                $capacity=($orders->capacity_left)-$quantity;
                $tb_quantity=($orders->order_quantity)+$quantity;
                $updated_created= date("Y-m-d H:i:s");
                DB::update('update orders set order_quantity = ? , capacity_left= ?, updated_at = ? where sku_id = ? and order_date = ?',[$tb_quantity,$capacity,$updated_created,$sku,$order_date]);
                Log::info("updated single non bundle order");
                
            }else{
                if(DB::table('inventories')->where('sku_id', $sku)->where('inventory_date', $order_date)->exists()){

                    $product_details=DB::table('inventories')->where('sku_id', $sku)->where('inventory_date', $order_date)->orderby('id','DESC')->first();
                    if($product_details == NULL){
                        $new_cap=50;
                    }else{
                        $new_cap=$product_details->order_capacity;
                    }
                }else{
                    $new_cap=30;
                }
                $capacity=$new_cap-$quantity;
                $updated_created= date("Y-m-d H:i:s");
                $values= array('sku_id'=> $sku,'order_date'=> $order_date,'order_quantity'=>$quantity,'capacity_left'=> $capacity,'updated_at'=>$updated_created,'created_at'=>$updated_created);
                DB::table('orders')->insert($values);
                Log::info("Inserted single non bundle order");

            }
        }else{
            parse_str($bundle_checker->bundled_sku,$bundles);
            foreach($bundles as $x => $x_value) {
                if(DB::table('orders')->where('order_date', $order_date)->where('sku_id', $x)->exists()){
                    $orders=DB::table('orders')->where('order_date', $order_date)->where('sku_id', $x)->first();
                    $capacity=($orders->capacity_left)-((int)$x_value*(int)$quantity);
                    $updated_created= date("Y-m-d H:i:s");
                    DB::update('update orders set  capacity_left= ? , updated_at = ? where sku_id = ? and order_date = ?',[$capacity,$updated_created,$x,$order_date]);
                    Log::info("updated bundle inside order");

                    
                }else{
                    if(DB::table('inventories')->where('sku_id', $x)->where('inventory_date', $order_date)->exists()){
    
                        $product_details=DB::table('inventories')->where('sku_id', $x)->where('inventory_date', $order_date)->orderby('id','DESC')->first();
                        if($product_details == NULL){
                            $new_cap=50;
                        }else{
                            $new_cap=$product_details->order_capacity;
                        }
                    }else{
                        $new_cap=30;
                    }
                    $capacity=$new_cap-((int)$x_value*(int)$quantity);
                    $updated_created= date("Y-m-d H:i:s");
                    $zero_quant=0;
                    $values= array('sku_id'=> $x,'order_date'=> $order_date,'order_quantity'=>$zero_quant,'capacity_left'=> $capacity,'updated_at'=>$updated_created,'created_at'=>$updated_created);
                    DB::table('orders')->insert($values);
                    Log::info("inserted bundle inside order");

                }

        }
        if(DB::table('orders')->where('order_date', $order_date)->where('sku_id', $sku)->exists()){
            $orders=DB::table('orders')->where('order_date', $order_date)->where('sku_id', $sku)->first();
            $capacity=($orders->capacity_left)-$quantity;
            $tb_quantity=($orders->order_quantity)+$quantity;
            $updated_created= date("Y-m-d H:i:s");
            DB::update('update orders set order_quantity = ? , capacity_left= ?, updated_at = ? where sku_id = ? and order_date = ?',[$tb_quantity,$capacity,$updated_created,$sku,$order_date]);
            Log::info("updated bundle main order");

            
        }else{
            if(DB::table('inventories')->where('sku_id', $sku)->where('inventory_date', $order_date)->exists()){

                $product_details=DB::table('inventories')->where('sku_id', $sku)->where('inventory_date', $order_date)->orderby('id','DESC')->first();
                if($product_details == NULL){
                    $new_cap=50;
                }else{
                    $new_cap=$product_details->order_capacity;
                }
            }else{
                $new_cap=100;
            }
            $capacity=$new_cap-$quantity;
            $updated_created= date("Y-m-d H:i:s");
            $values= array('sku_id'=> $sku,'order_date'=> $order_date,'order_quantity'=>$quantity,'capacity_left'=> $capacity,'updated_at'=>$updated_created,'created_at'=>$updated_created);
            DB::table('orders')->insert($values);
            Log::info("inserted bundle main order");

        }
    }
        }
        Log::info($action);
    }*/

    
    $json = file_get_contents('php://input');
    $action = json_decode($json, true);
    $count_note=count($action['note_attributes']);
    $order_date=date('Y-m-d', strtotime(' + 2 days'));
    for($i=0;$i<$count_note;$i++){
        if( $action['note_attributes'][$i]['name'] == "date"){
            $var = $action['note_attributes'][$i]['value'];
            $date = str_replace('/', '-', $var);
            $order_date=date('Y-m-d', strtotime($date));
        }
    }
    
    $order_id=$action['id'];
    $count_order=count($action['line_items']);
    for($i=0;$i<$count_order;$i++){
        $sku=$action['line_items'][$i]['sku'];
        $quantity=$action['line_items'][$i]['quantity'];
        $is_bundle=0;
        if(DB::table('products')->where('sku_id', $sku)->exists()){
        $bundle_checker=DB::table('products')->where('sku_id', $sku)->first();
        $is_bundle=$bundle_checker->bundled;
        }
        if($is_bundle == 0){
        if(DB::table('orders')->where('order_date', $order_date)->where('sku_id', $sku)->exists()){
            $orders=DB::table('orders')->where('order_date', $order_date)->where('sku_id', $sku)->first();
            $capacity=($orders->capacity_left)-$quantity;
            $tb_quantity=($orders->order_quantity)+$quantity;
            $updated_created= date("Y-m-d H:i:s"); 
            DB::update('update orders set order_quantity = ? , capacity_left= ?, updated_at = ? where sku_id = ? and order_date = ?',[$tb_quantity,$capacity,$updated_created,$sku,$order_date]);
            Log::info("updated single non bundle order");
            
        }else{
            if(DB::table('inventories')->where('sku_id', $sku)->where('inventory_date', $order_date)->exists()){

                $product_details=DB::table('inventories')->where('sku_id', $sku)->where('inventory_date', $order_date)->first();
                if($product_details == NULL){
                    $new_cap=50;
                }else{
                    $sum_of_invent=DB::table("inventories")->where('sku_id', $sku)->where('inventory_date', $order_date)->sum("order_capacity");
                    $new_cap=$sum_of_invent;
                }
            }else{
                $new_cap=30;
            }
            $capacity=$new_cap-$quantity;
            $updated_created= date("Y-m-d H:i:s"); 
            $values= array('sku_id'=> $sku,'order_date'=> $order_date,'order_quantity'=>$quantity,'capacity_left'=> $capacity,'updated_at'=>$updated_created,'created_at'=>$updated_created);
            DB::table('orders')->insert($values);
            Log::info("Inserted single non bundle order");

        }
    }else{
        parse_str($bundle_checker->bundled_sku,$bundles);
        foreach($bundles as $x => $x_value) {
            if(DB::table('orders')->where('order_date', $order_date)->where('sku_id', $x)->exists()){
                $orders=DB::table('orders')->where('order_date', $order_date)->where('sku_id', $x)->first();
                $capacity=($orders->capacity_left)-((int)$x_value*(int)$quantity);
                $updated_created= date("Y-m-d H:i:s"); 
                DB::update('update orders set  capacity_left= ? , updated_at = ? where sku_id = ? and order_date = ?',[$capacity,$updated_created,$x,$order_date]);
                Log::info("updated bundle inside order");

                
            }else{
                if(DB::table('inventories')->where('sku_id', $x)->where('inventory_date', $order_date)->exists()){

                    $product_details=DB::table('inventories')->where('sku_id', $x)->where('inventory_date', $order_date)->first();
                    if($product_details == NULL){
                        $new_cap=50;
                    }else{
                        $sum_of_invent=DB::table("inventories")->where('sku_id', $x)->where('inventory_date', $order_date)->sum("order_capacity");
                        $new_cap=$sum_of_invent;
                    }
                }else{
                    $new_cap=30;
                }
                $capacity=$new_cap-((int)$x_value*(int)$quantity);
                $updated_created= date("Y-m-d H:i:s"); 
                $zero_quant=0;
                $values= array('sku_id'=> $x,'order_date'=> $order_date,'order_quantity'=>$zero_quant,'capacity_left'=> $capacity,'updated_at'=>$updated_created,'created_at'=>$updated_created);
                DB::table('orders')->insert($values);
                Log::info("inserted bundle inside order");

            }

    }
    if(DB::table('orders')->where('order_date', $order_date)->where('sku_id', $sku)->exists()){
        $orders=DB::table('orders')->where('order_date', $order_date)->where('sku_id', $sku)->first();
        $capacity=($orders->capacity_left)-$quantity;
        $tb_quantity=($orders->order_quantity)+$quantity;
        $updated_created= date("Y-m-d H:i:s"); 
        DB::update('update orders set order_quantity = ? , capacity_left= ?, updated_at = ? where sku_id = ? and order_date = ?',[$tb_quantity,$capacity,$updated_created,$sku,$order_date]);
        Log::info("updated bundle main order");

        
    }else{
        if(DB::table('inventories')->where('sku_id', $sku)->where('inventory_date', $order_date)->exists()){

            $product_details=DB::table('inventories')->where('sku_id', $sku)->where('inventory_date', $order_date)->first();
            if($product_details == NULL){
                $new_cap=50;
            }else{
                $sum_of_invent=DB::table("inventories")->where('sku_id', $sku)->where('inventory_date', $order_date)->sum("order_capacity");
                $new_cap=$sum_of_invent;
                }
        }else{
            $new_cap=30;
        }
        $capacity=$new_cap-$quantity;
        $updated_created= date("Y-m-d H:i:s"); 
        $values= array('sku_id'=> $sku,'order_date'=> $order_date,'order_quantity'=>$quantity,'capacity_left'=> $capacity,'updated_at'=>$updated_created,'created_at'=>$updated_created);
        DB::table('orders')->insert($values);
        Log::info("inserted bundle main order");

    }
}
    }
    Log::info($action);
}
}