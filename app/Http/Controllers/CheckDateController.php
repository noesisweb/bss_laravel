<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use DateTime;

class CheckDateController extends Controller
{
   
    function index()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST");
        header("Access-Control-Allow-Headers: Origin,Content-Type,Authorization, Access-Control-Allow-Headers, X-Requested-With,X-Auth-Token");
        header('Content-Type:application/json');
        // Takes raw data from the request
        $json = file_get_contents('php://input');
       /* $json ='{ 
            "sku":{"col1":"1","col2":"2"}, 
            "date": "02-02-2020"
        }';*/

        // Converts it into a PHP object
       
        $data = array();
        $data['date'] = $_POST['date'];
        $data['sku'] = array();
        $data['sku'] = json_decode($_POST['sku']);
        $counts= count($data['sku']); 
        $var = $data['date'];
        $date = str_replace('/', '-', $var);
        $order_date=date('Y-m-d', strtotime($date));
        $sender=array();
        for($i=0;$i<$counts;$i++){
	        $sku=$data['sku'][$i]->sku_id;
            $quantity=$data['sku'][$i]->quantity;
            $is_bundle=0;
            if(DB::table('products')->where('sku_id', $sku)->exists()){
                $bundle_checker=DB::table('products')->where('sku_id', $sku)->first();
                $is_bundle=$bundle_checker->bundled;
                $title=$bundle_checker->title;
            }
            if($is_bundle == 0){
                if(DB::table('inventories')->where('sku_id', $sku)->where('inventory_date', $order_date)->exists()){
                    
                    $product_details=DB::table('inventories')->where('sku_id', $sku)->where('inventory_date', $order_date)->orderby('id','DESC')->first();
                    $order_quant=0;
                    $total_cap=DB::table("inventories")->where('sku_id', $sku)->where('inventory_date', $order_date)->sum("order_capacity");
                    if(DB::table('orders')->where('order_date', $order_date)->where('sku_id', $sku)->exists()){
                        $orders=DB::table('orders')->where('order_date', $order_date)->where('sku_id', $sku)->first();
                        $order_quant=$orders->order_quantity;
                        $order_left=$orders->capacity_left;
                    }
                    if(isset($order_left)){
                        if($order_left<$quantity){
	                        if($order_left>1){
                            	$sender[$title]["quantity"]=$order_left;
                            }else{
	                            $sender[$title]["quantity"]=$order_left; 
	                            $order_sub_date=$order_date;
                                for($i=0;$i<10;$i++){
                                    $datee=date_create($order_sub_date);
                                    $f_date=date_format(date_add($datee,date_interval_create_from_date_string("1 days")),"Y-m-d");
                                    if(DB::table('orders')->where('sku_id', $sku)->where('order_date', $f_date)->where('capacity_left','>=',$quantity)->exists()){
                                        $pd=DB::table('orders')->where('sku_id', $sku)->where('order_date', $f_date)->where('capacity_left','>=',$quantity)->first();
                                        $sender[$title]['future_date']=$pd->order_date;
                                        break;
                                    }else if(DB::table('inventories')->where('sku_id', $sku)->where('inventory_date', $f_date)->where('order_capacity','>=',$quantity)->exists()){
                                        $pdd=DB::table('inventories')->where('sku_id', $sku)->where('inventory_date', $f_date)->where('order_capacity','>=',$quantity)->first();
                                        $sender[$title]['future_date']=$pdd->inventory_date;
                                        break;
                                        }
                                    $order_sub_date=$f_date;
                                }
	                        }

                        }	
                    }else{
                    if(($total_cap-$order_quant)<$quantity){
                        $sender[$title]['quantity']=$total_cap-$order_quant;
                    }
                }

                }
            }else{
                parse_str($bundle_checker->bundled_sku,$bundles);
                $not_avail=0;
                $lowest_arr=[];
                $not_avail_date=0;
                $lowest_arr_date=[];
                 foreach($bundles as $x => $x_value) {
                    if(DB::table('inventories')->where('sku_id', $x)->where('inventory_date', $order_date)->exists()){

                        $product_details=DB::table('inventories')->where('sku_id', $x)->where('inventory_date', $order_date)->orderby('id','DESC')->first();
                        $order_quant=0;
                        $total_cap=DB::table("inventories")->where('sku_id', $x)->where('inventory_date', $order_date)->sum("order_capacity");
                        if(DB::table('orders')->where('order_date', $order_date)->where('sku_id', $x)->exists()){
                            $orders=DB::table('orders')->where('order_date', $order_date)->where('sku_id', $x)->first();
                            $order_quant=$orders->order_quantity;
                            $order_left=$orders->capacity_left;

                        }
                        if(isset($order_left)){
                            if($order_left<((int)$quantity*(int)$x_value)){
                                if($order_left/(int)$x_value>1){
                                array_push($lowest_arr,$order_left/(int)$x_value);
                                $not_avail=1;
                                }else{
	                                array_push($lowest_arr,$order_left/(int)$x_value);
                                    $not_avail=1;	                                		                          $order_sub_date=$order_date;
                                    for($i=0;$i<10;$i++){
                                        $datee=date_create($order_sub_date);
                                        $f_date=date_format(date_add($datee,date_interval_create_from_date_string("1 days")),"Y-m-d");
                                        if(DB::table('orders')->where('sku_id', $x)->where('order_date', $f_date)->where('capacity_left','>=',((int)$quantity*(int)$x_value))->exists()){
                                            $pd=DB::table('orders')->where('sku_id', $x)->where('order_date', $f_date)->where('capacity_left','>=',((int)$quantity*(int)$x_value))->first();
                                            array_push($lowest_arr_date,$pd->order_date);   
                                            $not_avail_date=1;
                                            break;
                                        }else if(DB::table('inventories')->where('sku_id', $x)->where('inventory_date', $f_date)->where('order_capacity','>=',((int)$quantity*(int)$x_value))->exists()){
                                            $pdd=DB::table('inventories')->where('sku_id', $x)->where('inventory_date', $f_date)->where('order_capacity','>=',((int)$quantity*(int)$x_value))->first();
                                            array_push($lowest_arr_date,$pdd->inventory_date);
                                            $not_avail_date=1;
                                            break;
                                        }
                                        $order_sub_date=$f_date;
                                    }                                 
                                }
                               // $sender[$title][$x]=$order_left;
                            }else if($not_avail_date==1){
	                            $order_sub_date=$order_date;
                                for($i=0;$i<10;$i++){
                                    $datee=date_create($order_sub_date);
                                    $f_date=date_format(date_add($datee,date_interval_create_from_date_string("1 days")),"Y-m-d");
                                    if(DB::table('orders')->where('sku_id', $x)->where('order_date', $f_date)->where('capacity_left','>=',((int)$quantity*(int)$x_value))->exists()){
                                        $pd=DB::table('orders')->where('sku_id', $x)->where('order_date', $f_date)->where('capacity_left','>=',((int)$quantity*(int)$x_value))->first();
                                        array_push($lowest_arr_date,$pd->order_date);   
                                        $not_avail_date=1;
                                        break;
                                    }else if(DB::table('inventories')->where('sku_id', $x)->where('inventory_date', $f_date)->where('order_capacity','>=',((int)$quantity*(int)$x_value))->exists()){
                                        $pdd=DB::table('inventories')->where('sku_id', $x)->where('inventory_date', $f_date)->where('order_capacity','>=',((int)$quantity*(int)$x_value))->first();
                                        array_push($lowest_arr_date,$pdd->inventory_date);
                                        $not_avail_date=1;
                                        break;
                                    }
                                    $order_sub_date=$f_date;
                                }               

                        }
                        }else{
                        if(($total_cap-$order_quant)<((int)$quantity*(int)$x_value)){
                            array_push($lowest_arr,($total_cap-$order_quant)/(int)$x_value);
                            $not_avail=1;
                            // $sender[$title][$x]=$total_cap-$order_quant;
                        }
                                            }   
   
                    }
                }
                if($not_avail==1){
                    $value_min=min($lowest_arr);
                    $sender[$title]['quantity']=(int)$value_min;
                }
                if($not_avail_date==1){
	                $value_min=max($lowest_arr_date);
                    $sender[$title]['future_date']=$value_min;
                }
            }
        }
        
        echo (json_encode($sender));
        exit();
    }

   
}