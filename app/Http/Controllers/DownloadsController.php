<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DownloadsController extends Controller
{
    //

    public function download() {
        $file_path = public_path('files/Bombay Sweet Shop excel Sample.xlsx');
        return response()->download($file_path);
      }
}
