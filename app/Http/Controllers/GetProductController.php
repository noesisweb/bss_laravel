<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use DateTime;

class GetProductController extends Controller
{
   
    function index()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST");
        header("Access-Control-Allow-Headers: Origin,Content-Type,Authorization, Access-Control-Allow-Headers, X-Requested-With,X-Auth-Token");
        header('Content-Type:application/json');
        // Takes raw data from the request
        $json = file_get_contents('php://input');
       /* $json ='{ 
            "sku":{"col1":"1","col2":"2"}, 
            "date": "02-02-2020"
        }';*/

        // Converts it into a PHP object
       
        $data = array();
        $data['date'] = $_GET['date'];
        $var = $data['date'];
        $date = str_replace('/', '-', $var);
        $order_date=date('Y-m-d', strtotime($date));
        $sender=array();
        if(DB::table('inventories')->where('inventory_date', $order_date)->exists()){
                    
            $product_details=DB::table('inventories')->where('inventory_date', $order_date)->where('order_capacity','>',0)->orderby('order_capacity','DESC')->get();
            $i=0;
            foreach($product_details as $pd){
                if(DB::table('orders')->where('order_date', $order_date)->where('sku_id', $pd->sku_id)->exists()){
                    $orders=DB::table('orders')->where('order_date', $order_date)->where('sku_id', $pd->sku_id)->first();
                    if(($orders->capacity_left)>0){
                        if(DB::table('products')->where('sku_id',$orders->sku_id)->exists()){
                            $bundle_checker=DB::table('products')->where('sku_id',$orders->sku_id)->first();
                            $title=$bundle_checker->title;
                            $sender['data'][$i]['product_name']=$title;
                            $sender['data'][$i]['product_sku']=$orders->sku_id;
                            $sender['data'][$i]['product_quantity']=$orders->capacity_left;
                            $i++;
                        }
                    }
            }else{
                if(DB::table('products')->where('sku_id',$pd->sku_id)->exists()){
                    $bundle_checker=DB::table('products')->where('sku_id',$pd->sku_id)->first();
                    $title=$bundle_checker->title;
                    $sender['data'][$i]['product_name']=$title;
                    $sender['data'][$i]['product_sku']=$pd->sku_id;
                    $sender['data'][$i]['product_quantity']=$pd->order_capacity;
                    $i++;
                }
            }
        
        }
    }
        echo (json_encode($sender));
        exit();
    }

   
}