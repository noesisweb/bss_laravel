<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
header('Access-Control-Allow-Origin:  *');
header('Access-Control-Allow-Methods:  POST, GET, OPTIONS, PUT, DELETE');
header('Access-Control-Allow-Headers:  Content-Type, X-Auth-Token, Origin, Authorization');
Route::get('/', function () {
    return view('welcome');
});
Route::post('/order/create', 'App\Http\Controllers\OrderCreatorController@import');
Route::post('/order/checkdate', 'App\Http\Controllers\CheckDateController@index');
Route::get('/order/getproducts', 'App\Http\Controllers\GetProductController@index');
Route::get('/download', 'App\Http\Controllers\DownloadsController@download');
//Route::get('/checkdate', 'App\Http\Controllers\CheckDateController@index');

 Route::get('loader', 'App\Http\Controllers\Admin\LoadDataController@index');
